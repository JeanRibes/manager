import { Component, OnInit } from '@angular/core';
import {UserService} from '../data/user.service';
import {Router} from '@angular/router';
import {NbSidebarService} from '@nebular/theme';
import {BilleventEvent, EventsService} from '../data/events.service';

@Component({
  selector: 'app-welcome-controller',
  templateUrl: './welcome-controller.component.html',
  styleUrls: ['./welcome-controller.component.scss']
})
export class WelcomeControllerComponent implements OnInit {

  user: any = {};
  events: BilleventEvent[];

  constructor(private sidebarService: NbSidebarService,
              private userService: UserService,
              private eventService: EventsService) {
  }

  ngOnInit() {
    this.eventService.getEvents().subscribe((events) => {this.events = events});
    this.userService.getCurrentUser()
      .subscribe((user: any) => this.user = user);
  }

}
