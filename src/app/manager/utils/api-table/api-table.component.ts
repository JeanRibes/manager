import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {HttpClient} from '@angular/common/http';

class Column {
  title: string;
  field: string;
}

@Component({
  selector: 'app-api-table',
  templateUrl: './api-table.component.html',
  styleUrls: ['./api-table.component.scss']
})
export class ApiTableComponent implements OnInit {

  @Input()
  config: any;

  @Output()
  select: EventEmitter<any> = new EventEmitter<any>(true);

  columns: Column[];
  endpoint: string;

  search: string;
  page = 1;
  lastApiResult: any;
  hasNext = false;
  hasPrevious = false;
  countByPage = 20;
  count = 0;
  entries: any[];

  constructor(private http: HttpClient) {
  }

  ngOnInit() {
    this.endpoint = this.config['endpoint'];
    this.columns = this.config['columns'];
    this.load();
  }

  private load() {
    const params = Object.assign({}, this.config.params || {});
    if (this.page !== 1) {
      params['page'] = this.page;
    }
    if (this.search && this.search !== '') {
      params['search'] = this.search;
    }
    this.http.get(this.endpoint, {params}).subscribe(
      (apiResult) => {
        this.lastApiResult = apiResult;
        const results: any[] = apiResult['results'];
        this.entries = results;
        this.hasNext = apiResult['next'] !== null;
        this.hasPrevious = apiResult['previous'] !== null;
        console.log(results);
      }
    );
  }

  clickOn(entry: any) {
    this.select.emit(entry);
  }

  makeSearch() {
    this.page = 1;
    this.load();
  }

  movePage(number: number) {
    if (number < 0 && this.hasPrevious) {
      this.page--;
      this.load();
    }
    if (number > 0 && this.hasNext) {
      this.page++;
      this.load();
    }
  }
}
