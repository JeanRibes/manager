import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {BilleventEvent} from './events.service';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class BilletsService {

  constructor(private httpClient: HttpClient) {
  }

  countSoldByDay(event: BilleventEvent, productsIds: number[]): Observable<any> {
    return this.httpClient.get(environment.BilleventApi + '/admin/billets/countByDay/', {
      params: {
        'status': 'validated',
        'event': event.id.toString(10) || '',
        'products': productsIds.join(',')
      }
    }).map((b) => b);
  }

}
