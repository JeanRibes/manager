import {Injectable} from '@angular/core';
import {BilleventEvent, Product} from './events.service';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {Observable} from 'rxjs/Observable';

export class Client {

}

export class Participant {
}

export class Option {
  id: number;
  name: string;
  type: string;
  price_ht: string;
  price_ttc: string;
  seats: number;
  target: string;
  event: number;
  price: number;
}

export class BilletOption {
  id: number;
  option: Option;
  participant: number;
  amount: number;
  billet: Billet;
}

export class Billet {
  id: number;
  product: Product;
  billet_options: BilletOption[];
  participants: Participant[];
  order: Order;
}

export class Answer {
}

export class Question {
}


export class Order {
  id: number;
  client: Client;
  event: BilleventEvent;
  billets: Billet[];
  status: string;
  amount: number;
  answers: Answer[];
}

@Injectable()
export class OrdersService {

  constructor(private httpClient: HttpClient) {
  }

  list() {
    return this.httpClient.get(environment.BilleventApi + '/admin/orders');
  }

  get(id: any): Observable<Order> {
    return this.httpClient.get<Order>(environment.BilleventApi + '/admin/orders/' + id + '/');
  }
}
