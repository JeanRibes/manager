import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Question} from '../../data/orders.service';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../environments/environment';
import {ExcelService} from '../../utils/excel.service';

@Component({
  selector: 'app-question',
  templateUrl: './question.component.html',
  styleUrls: ['./question.component.scss']
})
export class QuestionComponent implements OnInit {

  loadingAnswers = false;
  question: Question;

  constructor(private currentRoute: ActivatedRoute,
              private http: HttpClient,
              private excel: ExcelService) {
  }

  ngOnInit() {
    const id = this.currentRoute.snapshot.params['id'];
    this.http.get(environment.BilleventApi + '/admin/questions/' + id + '/').subscribe((question) => {
      this.question = <Question>question;
    });
  }

  downloadAnswers() {
    if (this.loadingAnswers) {
      return;
    }
    if (this.question['target'] !== 'Order' && this.question['target'] !== 'Participant') {
      alert('Je ne sais exporter que les questions de commande et participant pour l\'instant.');
      return;
    }
    this.loadingAnswers = true;
    this.http.get(environment.BilleventApi + '/admin/answers/', {params: {question: this.question['id']}}).subscribe((answers: any[]) => {
      const lines = [];
      answers.forEach((answer) => {
        if (this.question['target'] === 'Order') {
          lines.push({
            id: answer['id'],
            prenom: answer['order']['client']['first_name'],
            nom: answer['order']['client']['last_name'],
            commande: answer['order']['id'],
            nombre_places: answer['order']['billets'].reduce(
              (accumulator, billet) =>
                accumulator + billet.participants.length
              , 0),
            reponse: answer['value']
          });
        } else if (this.question['target'] === 'Participant') {
          lines.push({
            id: answer['id'],
            commande: answer['order']['id'],
            participant: answer['participant']['id'],
            prenom: answer['participant']['first_name'],
            nom: answer['participant']['last_name'],
            phone: answer['participant']['phone'],
            reponse: answer['value']
          });
        }
      });
      this.excel.exportAsExcelFile(lines, 'answers_' + this.question['id']);
      this.loadingAnswers = false;
    }, () => {
      alert('Erreur lors du chargement des réponses');
      this.loadingAnswers = false;
    });
  }
}
