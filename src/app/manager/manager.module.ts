import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ManagerRoutingModule} from './manager-routing.module';
import {DashboardComponent} from './dashboard/dashboard.component';
import {NbActionsModule, NbCardModule, NbLayoutModule, NbMenuModule, NbSidebarModule, NbSidebarService, NbUserModule} from '@nebular/theme';
import {NbUser} from '@nebular/auth/models/user';
import {HeaderComponent} from './layout/header/header.component';
import {UserService} from './data/user.service';
import {JwtModule} from '@auth0/angular-jwt';
import {InvitationComponent, InvitationLinkComponent} from './invitation/invitation.component';
import {HomeComponent} from './home/home.component';
import {InvitationsTableService} from './invitation/invitations.service';
import {EventsService} from './data/events.service';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { NewInvitationComponent } from './invitation/new-invitation/new-invitation.component';
import { EditInvitationComponent } from './invitation/edit-invitation/edit-invitation.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {InvitationsService} from './data/invitations.service';
import { ChartSellsByProductComponent } from './chart/chart-sells-by-product/chart-sells-by-product.component';
import {OrdersService} from './data/orders.service';
import {BilletsService} from './data/billets.service';
import { WelcomeControllerComponent } from './welcome-controller/welcome-controller.component';
import {NgxChartsModule} from '@swimlane/ngx-charts';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {HTTP_INTERCEPTORS} from '@angular/common/http';
import {UnauthorizedInterceptor} from './unauthorized.interceptor';
import { OrdersComponent } from './orders/orders.component';
import { ApiTableComponent } from './utils/api-table/api-table.component';
import { OrderComponent } from './orders/order/order.component';
import { QuestionsComponent } from './questions/questions.component';
import { QuestionComponent } from './questions/question/question.component';
import {ExcelService} from './utils/excel.service';
import {RoundLowPipe} from './utils/round-low.pipe';


@NgModule({
  imports: [
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    CommonModule,
    NbActionsModule,
    NbLayoutModule,
    NbSidebarModule,
    NbMenuModule,
    NbCardModule,
    NbUserModule,
    ManagerRoutingModule,
    Ng2SmartTableModule,
    NgxChartsModule
  ],
  providers: [
    NbSidebarService,
    UserService,
    ...NbMenuModule.forRoot().providers,
    InvitationsTableService,
    InvitationsService,
    EventsService,
    OrdersService,
    BilletsService,
    ExcelService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: UnauthorizedInterceptor,
      multi: true
    },
  ],
  declarations: [
    DashboardComponent,
    HeaderComponent,
    InvitationComponent,
    HomeComponent,
    InvitationLinkComponent,
    NewInvitationComponent,
    EditInvitationComponent,
    ChartSellsByProductComponent,
    WelcomeControllerComponent,
    OrdersComponent,
    ApiTableComponent,
    OrderComponent,
    QuestionsComponent,
    QuestionComponent,
    RoundLowPipe,
  ]
})
export class ManagerModule {
}
